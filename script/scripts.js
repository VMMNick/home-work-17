const student = {};
student.name = prompt('Введіть ім\'я студента:');
student.lastName = prompt('Введіть прізвище студента:');
student.tabel = {};
while (true) {
  const subject = prompt('Введіть назву предмету:');
  if (subject === null) {
    break;
  }
  const grade = parseFloat(prompt('Введіть оцінку за предмет:'));
  student.tabel[subject] = grade;
}
let poorGradesCount = 0;
for (const subject in student.tabel) {
  const grade = student.tabel[subject];
  if (grade < 4) {
    poorGradesCount++;
  }
}
let totalGrade = 0;
for (const subject in student.tabel) {
  totalGrade += student.tabel[subject];
}
const averageGrade = totalGrade / Object.keys(student.tabel).length;
if (poorGradesCount === 0) {
  console.log('Студент переведено на наступний курс');
} else {
  console.log('Студент не переведено на наступний курс');
}
if (averageGrade > 7) {
  console.log('Студенту призначено стипендію');
}